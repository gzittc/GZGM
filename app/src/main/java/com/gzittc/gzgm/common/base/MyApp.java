package com.gzittc.gzgm.common.base;

import org.litepal.LitePalApplication;

/**
 * 应用程序上下文对象，常作一些初始化操作
 * @author YZC
 * created at 2017-12-09
 */

public class MyApp extends LitePalApplication {
	
	@Override
	public void onCreate() {
		super.onCreate();
		Global.init(this);

	}

}
