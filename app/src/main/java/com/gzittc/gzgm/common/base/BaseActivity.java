package com.gzittc.gzgm.common.base;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Message;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.gzittc.gzgm.R;
import com.gzittc.gzgm.common.util.Utils;


/**
 * Activity基类，所有的Activity都需要继承此类。
 * 封装： 查看子控件，设置监听器，初始化数据，
 * toast, showDialog, showProgressDialog等方法
 * @author YZC
 * created at 2017-12-09
 */
public abstract class BaseActivity extends AppCompatActivity
		implements IUIOperation,BaseView {

	private ImageView view;

	/** 访问网络接口实现，处理错误信息 */
	@Override
	public void onHttpSuccess(int reqtype,Message msg) {
		//null
	}

	@Override
	public void onHttpError( String error) {
		showToast("Http请求失败："+error);
	}

	public View mRoot;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(getLayoutRes());

		// 系统的一个根布局，可以查找到activity布局的所有的子控件
		mRoot = findViewById(android.R.id.content);

		// 查找activity布局中所有的Button（ImageButton），并设置点击事件
		Utils.findButtonAndSetOnClickListener(mRoot, this);

		initView();
		initListener();
		initData();
	}

	/** 查找子控件，可以省略强转 */
	public <T> T findView(int id) {
		@SuppressWarnings("unchecked")
		T view = (T) findViewById(id);
		return view;
	}

	public BaseActivity() {

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.img_back://返回键用这个id写
				finish();
				break;
			default:
				onClick(v, v.getId());
				break;
		}
	}

	@Override
	public void initListener() {
		try {
			view = (ImageView) findViewById(R.id.img_back);
			view.setOnClickListener(this);
		}catch (NullPointerException n){

		}
	}

	public void showToast(String text) {
		Global.showToast(text);
	}

	private ProgressDialog mPDialog;

	/**
	 * 显示加载提示框(不能在子线程调用)
	 */
	public void showProgressDialog(final String message) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				mPDialog = new ProgressDialog(BaseActivity.this);
				mPDialog.setMessage(message);
				// 点击外部时不销毁
				mPDialog.setCanceledOnTouchOutside(false);

				// activity如果正在销毁或已销毁，不能show Dialog，否则出错。
				if (!isFinishing())
					mPDialog.show();
			}
		});
	}

	protected void setPageTitle(String title) {
		TextView tvTitle = findView(R.id.tv_title);//标题统一id名
		if (tvTitle != null) {
			tvTitle.setText(title);
		}
	}

	/**
	 * 销毁加载提示框
	 */
	public void dismissProgressDialog() {
		if (mPDialog != null) {
			mPDialog.dismiss();
			mPDialog = null;
		}
	}

	/**
	 * 显示对话框
	 *
	 * @param title 标题
	 * @param message 内容
	 * @param listener 回调监听器
	 */
	public void showDialog(String title, String message,
						   final OnDialogClickListener listener,String ok,String cancle) {

//		String ok ;
//		String cancle ;

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(title);
		builder.setMessage(message);
		builder.setPositiveButton(ok,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						if (listener != null) {
							listener.onConfirm(dialog);
						}
					}
				});
		builder.setNegativeButton(cancle, null);
		if (!isFinishing())
			builder.create().show();
	}

	/** 提示对话框*/
	public void showDialog(String title, String message,final OnDialogClickListener listener) {
		showDialog(title, message, listener,"确认","取消");
	}

	/** 提示对话框*/
	public void showDialog(String message,final OnDialogClickListener listener) {
		showDialog("", message, listener,"确认","取消");
	}

	/** 对话框点击回调 */
	public interface OnDialogClickListener {

		/** 确定 */
		void onConfirm(DialogInterface dialog);

		/** 取消 */
		void onCancel(DialogInterface dialog) ;
	}
	@Override
	public Resources getResources() {
		Resources res = super.getResources();
		Configuration config = new Configuration();
		config.setToDefaults();
		res.updateConfiguration(config, res.getDisplayMetrics());
		return res;
	}
}



















