package com.gzittc.gzgm.common.util;

import android.content.Context;

import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.gzittc.gzgm.common.base.Global;

/**
 * Created by Administrator on 2017/12/18.
 */

public class SliderLayoutUtil {
    SliderLayout sliderLayout;
    Context context;
    public SliderLayoutUtil(SliderLayout sliderLayout, Context context){
        this.sliderLayout = sliderLayout;
        this.context = context;
    }
    public void slAdd(final String imageUrl){
        DefaultSliderView defaultSliderView = new DefaultSliderView(context);

        // imgUrl: 轮播图图片的Url
        defaultSliderView.image(imageUrl);
        // 轮播图子界面点击事件
        defaultSliderView.setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
            @Override
            public void onSliderClick(BaseSliderView slider) {
                //todo
                Global.showToast(imageUrl+"轮播图片");
            }
        });
        sliderLayout.addSlider(defaultSliderView);
    }
    public void removeAllViews(){
        sliderLayout.removeAllSliders();
    }
}
