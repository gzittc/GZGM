package com.gzittc.gzgm.common.base;


import com.gzittc.gzgm.gm.model.protocol.BaseProtocol;


/**
 * mvp的View层接口
 * @author YZC
 * created at 2017-12-09
 */
public interface BaseView extends BaseProtocol.HttpCallback {

}
