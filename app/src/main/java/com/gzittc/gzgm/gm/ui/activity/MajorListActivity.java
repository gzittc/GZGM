package com.gzittc.gzgm.gm.ui.activity;

import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.gzittc.gzgm.R;
import com.gzittc.gzgm.common.base.BaseActivity;
import com.gzittc.gzgm.common.util.GsonUtils;
import com.gzittc.gzgm.gm.model.bean.requestbean.MajorListRequestBean;
import com.gzittc.gzgm.gm.presenter.MajorListPresenter;
import com.gzittc.gzgm.gm.ui.adapter.MajorListAdapter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by 12236 on 2017/12/18.
 */

public class MajorListActivity extends BaseActivity {


    @BindView(R.id.lv_major_listview)
    ListView lvMajorListview;
    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.tv_title)
    TextView tvToolbarTitle;

    private MajorListPresenter majorListPresenter;

    private MajorListAdapter adapter;

    @Override
    public void onHttpSuccess(int reqtype, Message msg) {
        adapter = new MajorListAdapter(this, (List) msg.obj);
        lvMajorListview.setAdapter(adapter);

    }


    @Override
    public void onHttpError(String error) {
        super.onHttpError(error);
    }

    @Override
    public int getLayoutRes() {
        return R.layout.activity_major_list;
    }

    @Override
    public void initView() {
        ButterKnife.bind(this);
        majorListPresenter = new MajorListPresenter(this);
        initMajorList();
        tvToolbarTitle.setText(getIntent().getStringExtra("department"));//Toobar 对于专业title
       /* imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });*/

    }

    //专业列表请求
    private void initMajorList() {
        MajorListRequestBean bean = new MajorListRequestBean();
        bean._a = "index";
        bean._c = "specialty";
        bean.department_id = getIntent().getStringExtra("department_id"); //系id
        String json = GsonUtils.objectToString(bean);
        majorListPresenter.getMajorList(json, 3);
//        protocol.getHomeBanner(json, this,1);

    }

    @Override
    public void initListener() {
        super.initListener();
    }

    @Override
    public void initData() {

    }

    @Override
    public void onClick(View v, int id) {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }
}
