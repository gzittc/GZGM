package com.gzittc.gzgm.gm.ui.activity;

import android.support.design.widget.TabLayout;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.gzittc.gzgm.R;
import com.gzittc.gzgm.common.base.BaseActivity;
import com.gzittc.gzgm.gm.presenter.MainActivityPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.gzittc.gzgm.common.base.Global.mContext;


/**
 * Created by yzc on 2017-12-09.
 */

public class MainActivity extends BaseActivity {
    @BindView(R.id.tl_main)
    TabLayout tlMain;
    @BindView(R.id.homefragment)
    RelativeLayout home;
    @BindView(R.id.myfragment)
    RelativeLayout my;

    private MainActivityPresenter map;

    @Override
    public int getLayoutRes() {
        return R.layout.activity_main;
    }

    @Override
    public void initView() {
//        Global.setNoStatusBarFullMode(this);
        ButterKnife.bind(this);
        //2. 初始化Tablayout
        initTablayout();
        initDatabase();//初始化数据库
    }


    private void initDatabase() {
        map = new MainActivityPresenter();
        map.processingPDTestDatabase(getApplicationContext());
    }

    String[] tabs = mContext.getResources().getStringArray(R.array .tabs);; //标签的标题数组
    //2. 初始化Tablayout
    private void initTablayout() {
        tlMain.addTab(tlMain.newTab().setText(tabs[0]).setIcon(R.drawable.tab_main_selector));
        tlMain.addTab(tlMain.newTab().setText(tabs[1]).setIcon(R.drawable.tab_notebook_selector));
        tlMain.addTab(tlMain.newTab().setText(tabs[2]).setIcon(R.drawable.tab_me_selector));

    }

    @Override
    public void initListener() {
        tlMain.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if(tab.getPosition() == 0){
                    home.setVisibility(View.VISIBLE);
                    my.setVisibility(View.INVISIBLE);
                }else if(tab.getPosition() == 1){
                    showToast("暂时没有");
                }else{
                    home.setVisibility(View.INVISIBLE);
                    my.setVisibility(View.VISIBLE);
                }
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }
            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    public void initData() {
    }

    @Override
    public void onClick(View v, int id) {

    }

}
