package com.gzittc.gzgm.gm.ui.fragment;

import android.content.Intent;
import android.os.Message;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import com.daimajia.slider.library.SliderLayout;
import com.gzittc.gzgm.R;
import com.gzittc.gzgm.common.base.BaseFragment;
import com.gzittc.gzgm.common.util.GsonUtils;
import com.gzittc.gzgm.common.util.SliderLayoutUtil;
import com.gzittc.gzgm.gm.model.bean.databean.HomeQuestionBean;
import com.gzittc.gzgm.gm.model.bean.requestbean.BaseRequestBean;
import com.gzittc.gzgm.gm.model.db.bean.PSExam;
import com.gzittc.gzgm.gm.presenter.HomeFragmentPresenter;
import com.gzittc.gzgm.gm.ui.activity.ExaminationPrepareActivity;
import com.gzittc.gzgm.gm.ui.activity.MajorListActivity;
import com.gzittc.gzgm.gm.ui.activity.WebViewActivity;
import com.gzittc.gzgm.gm.ui.adapter.HomeQuestionListAdapter;
import com.gzittc.gzgm.gm.ui.view.Mydialog;

import org.litepal.crud.DataSupport;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Administrator on 2017/12/12.
 */

public class HomeFragment extends BaseFragment {
    @BindView(R.id.banner)
    SliderLayout banner;
//    @BindView(R.id.ask_list_view)
//    MyListView askListView;
    @BindView(R.id.wenchuang)
    LinearLayout wenchuang;
    @BindView(R.id.shangmao)
    LinearLayout shangmao;
    @BindView(R.id.xinnengyuan)
    LinearLayout xinnengyuan;
    @BindView(R.id.xinxi)
    LinearLayout xinxi;
    @BindView(R.id.zhizao)
    LinearLayout zhizao;
    @BindView(R.id.school_synopsis)
    LinearLayout schoolSynopsis;
    @BindView(R.id.college_landscape)
    LinearLayout collegeLandscape;
    @BindView(R.id.transportation_guide)
    LinearLayout transportationGuide;
    @BindView(R.id.enrol)
    LinearLayout enrol;
    @BindView(R.id.consulting)
    LinearLayout consulting;
    SliderLayoutUtil slUtil;//轮播图的工具类
    @BindView(R.id.scrollView)
    ScrollView scrollView;
    @BindView(R.id.kaoshi)
    LinearLayout kaoshi;

    private HomeFragmentPresenter presenter;//p层
    private BaseRequestBean bean = new BaseRequestBean();
    ;//请求bean
    private HomeQuestionListAdapter adapter;//list view的适配器
    private List<HomeQuestionBean.DataBean.QuestionListBean> questionListBeen;//访问网络拿到的数据

    private String json;//请求时的参数
    private Intent intent;

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_home;
    }

    /**
     * 初始化视图
     */
    @Override
    public void initView() {
        ButterKnife.bind(this, mRoot);
//        Global.setNoStatusBarFullMode(getActivity());
        presenter = new HomeFragmentPresenter(this);
        slUtil = new SliderLayoutUtil(banner, getContext());
        initBanner();
        initQuestion();
    }

    private void initBanner() {
        bean._a = "index";
        bean._c = "index";
        json = GsonUtils.objectToString(bean);
        presenter.getHomeBanner(json, 1);
    }

    private void initQuestion() {
        bean._a = "index";
        bean._c = "question";
        json = GsonUtils.objectToString(bean);
        presenter.getHomeQuestion(json, 2);
    }

    @Override
    public void initListener() {

    }

    @Override
    public void initData() {

    }

    @Override
    public void onClick(View v, int id) {


    }

    //这里处理得到的数据
    @Override

    public void onHttpSuccess(int reqtype, Message msg) {
        switch (reqtype) {
            case 1:
                List<String> imgUrl = (List<String>) msg.obj;
                slUtil.removeAllViews();
                for (String s : imgUrl)
                    slUtil.slAdd(s);
                break;
            case 2:
                questionListBeen = (List<HomeQuestionBean.DataBean.QuestionListBean>) msg.obj;
//                adapter = new HomeQuestionListAdapter(getActivity(), questionListBeen);
//                askListView.setAdapter(adapter);
                break;
        }

//        Toast.makeText(mActivity, h.getData().getCz_banner().getUrl(), Toast.LENGTH_SHORT).show();
    }

    //处理出错的信息
    @Override
    public void onHttpError(String error) {

    }


    @OnClick({R.id.wenchuang, R.id.shangmao, R.id.xinnengyuan, R.id.xinxi, R.id.zhizao, R.id.school_synopsis, R.id.college_landscape, R.id.kaoshi})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.wenchuang:
                intent = new Intent(mActivity, MajorListActivity.class);
                intent.putExtra("department", "文化创意");
                intent.putExtra("department_id", "1");
                startActivity(intent);
                break;
            case R.id.shangmao:
                intent = new Intent(mActivity, MajorListActivity.class);
                intent.putExtra("department", "贸易服装");
                intent.putExtra("department_id", "2");
                startActivity(intent);
                break;
            case R.id.xinnengyuan:
                intent = new Intent(mActivity, MajorListActivity.class);
                intent.putExtra("department", "新能源应用");
                intent.putExtra("department_id", "3");
                startActivity(intent);
                break;
            case R.id.xinxi:
                intent = new Intent(mActivity, MajorListActivity.class);
                intent.putExtra("department", "信息服务");
                intent.putExtra("department_id", "4");
                startActivity(intent);
                break;
            case R.id.zhizao:
                intent = new Intent(mActivity, MajorListActivity.class);
                intent.putExtra("department", "先进制造");
                intent.putExtra("department_id", "5");
                startActivity(this.intent);
                break;
            case R.id.kaoshi://考试跳转(判断是否登陆，未登录弹窗提示登陆，登陆获取信息跳转至确认页面)
                Exam_showdialog();
                break;
            case R.id.school_synopsis:
                intent = new Intent(getActivity(),WebViewActivity.class);
                intent.putExtra("id", "schoolDesc");
                startActivity(intent);
                break;
            case R.id.college_landscape:
                intent = new Intent(getActivity(),WebViewActivity.class);
                intent.putExtra("id", "images");
                startActivity(intent);
                break;
        }

    }

    private Mydialog mydialog;
    private void Exam_showdialog() {
        mydialog = new Mydialog(mActivity);
        mydialog.setYesOnClickListener(new Mydialog.YesOnClickListener() {
            @Override
            public void YesOnClick() {
                PrepareExam(mydialog.getCur());
                mydialog.dismiss();
            }
        });
        mydialog.setNoOnClickListener(new Mydialog.NoOnClickListener() {
            @Override
            public void NoOnClick() {
                mydialog.dismiss();
            }
        });
        mydialog.show();
    }

    private void PrepareExam(int cur) {
        intent = new Intent(getActivity(), ExaminationPrepareActivity.class);
        String sqlName = "";
        String subject = "";
        String examName = "";
        String time = "";
        String many = "";
        switch(cur){
            case 0:
                sqlName = "17ps_exam";
                subject = "计算机程序设计（移动互联网开发）";
                examName = "PS理论测试";
                time="30";
                many = DataSupport.findAll(PSExam.class).size()+"题";
                break;
            case 1:
                sqlName = "AndroidCase";
                subject = "计算机程序设计（移动互联网开发）";
                examName = "Android开发基础";
                time="30";
                many = "50题";
                break;
            default:
                break;
        }
        intent.putExtra("sql",sqlName);//哪个数据库
        intent.putExtra("subject",subject);
        intent.putExtra("content",examName);
        intent.putExtra("time",time);
        intent.putExtra("many", many);
        startActivity(intent);
    }

}
