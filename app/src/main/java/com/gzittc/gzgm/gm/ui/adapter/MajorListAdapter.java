package com.gzittc.gzgm.gm.ui.adapter;

import android.content.Context;
import android.view.ViewGroup;

import com.gzittc.gzgm.common.ui.BaseAdapterLV;
import com.gzittc.gzgm.common.ui.BaseHolderLV;
import com.gzittc.gzgm.gm.model.bean.databean.MajorListBean;
import com.gzittc.gzgm.gm.ui.holder.MajorListHolder;

import java.util.List;

/**
 * Created by 12236 on 2017/12/18.
 */

public class MajorListAdapter extends BaseAdapterLV<MajorListBean.DataBean.SpecialtyDataBean> {

    public MajorListAdapter(Context context, List listData) {
        super(context, listData);
    }

    @Override
    public BaseHolderLV createViewHolder(Context context, ViewGroup parent, MajorListBean.DataBean.SpecialtyDataBean bean, int position) {
        return new MajorListHolder(context,parent,this,position,bean);
    }

}
