package com.gzittc.gzgm.gm.model.db.bean.Case;

/**
 * Created by yzc on 2018-04-08.
 */

public class Part {
    private int type;
    private int part;
    private String partname;

    public Part(int type, int part, String partname) {
        this.type = type;
        this.part = part;
        this.partname = partname;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getPart() {
        return part;
    }

    public void setPart(int part) {
        this.part = part;
    }

    public String getPartname() {
        return partname;
    }

    public void setPartname(String partname) {
        this.partname = partname;
    }
}
