package com.gzittc.gzgm.gm.presenter;

import android.os.Message;

import com.gzittc.gzgm.common.base.BaseView;
import com.gzittc.gzgm.gm.model.protocol.BaseProtocol;
import com.gzittc.gzgm.gm.model.protocol.IHttpService;

/**
 * Created by Administrator on 2017/12/21.
 */

public class CollegeSynopsisPresenter extends BasePresenter {
    public CollegeSynopsisPresenter(BaseView baseView) {
        super(baseView);
    }

    public void getCollegeSynopsis(String _param,int reqtype){
        protocol.getCollegeSynopsis(_param, subCallback, reqtype);
    }
    public void getCollegeScenery(String _param,int reqtype){
        protocol.getCollegeScenery(_param, subCallback, reqtype);
    }

    BaseProtocol.HttpCallback subCallback=new BaseProtocol.HttpCallback() {
        @Override
        public void onHttpSuccess(int reqType, Message msg) {
            if(reqType== IHttpService.GET_COllEGE_SYNOPSIS){
                // 数据解析出来的数据，进行格式转换后再回调显示
//                msg.obj = CollegeSynopsisData((CollegeSynopsisBean) msg.obj);
//                msg.obj = (CollegeSynopsisBean) msg.obj;
                baseView.onHttpSuccess(reqType, msg);
                return;
            }
            baseView.onHttpSuccess(reqType,msg);
        }

        @Override
        public void onHttpError( String error) {
            baseView.onHttpError(error);
        }
    };

//    private Object CollegeSynopsisData(CollegeSynopsisBean obj) {
//        obj.getData().getSchool_desc();
//        return obj;
//    }
}
