package com.gzittc.gzgm.gm.model.bean.requestbean;

/**
 * 这个基础类可完成大部分请求
 * 有其他参数需求的可继承此类，其他参数自行设置
 * Created by Administrator on 2017/12/15.
 * author :  yyx
 */

public class BaseRequestBean {
    public String _a;
    public String _c;
    public String _debug = "1";//默认为1，请求时可不用设置

}
