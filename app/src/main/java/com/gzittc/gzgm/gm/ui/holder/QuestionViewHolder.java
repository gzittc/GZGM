package com.gzittc.gzgm.gm.ui.holder;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.gzittc.gzgm.R;
import com.gzittc.gzgm.common.ui.BaseAdapterLV;
import com.gzittc.gzgm.common.ui.BaseHolderLV;
import com.gzittc.gzgm.gm.model.bean.databean.HomeQuestionBean;

/**
 * Created by Administrator on 2017/12/19.
 */

public class QuestionViewHolder extends BaseHolderLV<HomeQuestionBean.DataBean.QuestionListBean> {

    private TextView question;
    private TextView answer;

    public QuestionViewHolder(Context context, ViewGroup parent, BaseAdapterLV adapter, int position, HomeQuestionBean.DataBean.QuestionListBean bean) {
        super(context, parent, adapter, position, bean);

    }

    @Override
    public View onCreateView(Context context, ViewGroup parent) {
        View view = inflater.inflate(R.layout.home_mylistview_item,parent,false);

        question = view.findViewById(R.id.list_item_question);
        answer = view.findViewById(R.id.list_item_answer);

        return view;
    }

    @Override
    protected void onRefreshView(HomeQuestionBean.DataBean.QuestionListBean bean, int position) {
        question.setText(bean.getQuestion_content());
        answer.setText(bean.getAnswer_content());
    }
}
