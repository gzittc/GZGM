package com.gzittc.gzgm.gm.ui.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.gzittc.gzgm.R;
import com.gzittc.gzgm.common.base.Global;

import java.util.List;

/**
 * Created by Administrator on 2017/12/12.
 */

public class MainFragmentAdapter extends FragmentPagerAdapter {
    List<Fragment> fragments;
    String[] tabs=null; //标签的标题数组
    public MainFragmentAdapter(FragmentManager fm, List<Fragment> fragments) {
        super(fm);
        this.fragments = fragments;
        tabs= Global.mContext.getResources().getStringArray(R.array.tabs);
    }

    @Override
    public Fragment getItem(int position) {
        return fragments==null?null:fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments==null?0:fragments.size();
    }
    //取得页面标题，TabLayout的页签的标题要依赖该方法
    @Override
    public CharSequence getPageTitle(int position) {
        return tabs[position];
    }
}
