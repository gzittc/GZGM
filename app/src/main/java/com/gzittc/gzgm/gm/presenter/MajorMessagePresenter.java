package com.gzittc.gzgm.gm.presenter;

import android.os.Message;

import com.gzittc.gzgm.common.base.BaseView;
import com.gzittc.gzgm.gm.model.bean.databean.MajorMessageBean;
import com.gzittc.gzgm.gm.model.protocol.BaseProtocol;
import com.gzittc.gzgm.gm.model.protocol.IHttpService;

/**
 * Created by 12236 on 2017/12/19.
 */

public class MajorMessagePresenter extends BasePresenter {
    
    public MajorMessagePresenter(BaseView baseView) {
        super(baseView);
    }
    public void getMajorMessage(String _param,int reqtype){
        protocol.getMajorMessage(_param, subCallback, reqtype);
    }

    BaseProtocol.HttpCallback subCallback=new BaseProtocol.HttpCallback() {
        @Override
        public void onHttpSuccess(int reqType, Message msg) {
            if(reqType== IHttpService.GET_MAJOR_MESSAGE){
                // 数据解析出来的数据，进行格式转换后再回调显示
//                msg.obj = MajorMessageData((MajorMessageBean) msg.obj);
                baseView.onHttpSuccess(reqType, msg);
                return;
            }
            baseView.onHttpSuccess(reqType,msg);
        }

        @Override
        public void onHttpError( String error) {
            baseView.onHttpError(error);
        }
    };

    private MajorMessageBean.DataBean.SpecialtyInfoBean MajorMessageData(MajorMessageBean obj){
//        obj.getData().getSpecialty_info();
        return obj.getData().getSpecialty_info();
    }


}
