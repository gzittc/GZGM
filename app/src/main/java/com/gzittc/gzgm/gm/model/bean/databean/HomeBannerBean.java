package com.gzittc.gzgm.gm.model.bean.databean;

import java.util.List;

/**
 * Created by Administrator on 2017/12/13.
 */

public class HomeBannerBean {

    /**
     * code : 0
     * msg : 成功
     * data : {"cz_banner":{"url":"http://gm.cchtw.com/images/ic_banner1.png","data":["http://gm.cchtw.com/images/gm1.jpg","http://gm.cchtw.com/images/gm2.jpg","http://gm.cchtw.com/images/gm3.jpg","http://gm.cchtw.com/images/gm4.jpg","http://gm.cchtw.com/images/cz1.jpg","http://gm.cchtw.com/images/cz2.jpg","http://gm.cchtw.com/images/cz3.jpg","http://gm.cchtw.com/images/cz4.jpg","http://gm.cchtw.com/images/cz5.jpg","http://gm.cchtw.com/images/cz6.jpg","http://gm.cchtw.com/images/cz7.jpg","http://gm.cchtw.com/images/cz8.jpg","http://gm.cchtw.com/images/cz9.jpg","http://gm.cchtw.com/images/cz10.jpg","http://gm.cchtw.com/images/cz11.jpg","http://gm.cchtw.com/images/cz12.jpg"]},"gz_banner":{"url":"http://gm.cchtw.com/images/ic_banner2.png","data":["http://gm.cchtw.com/images/gm1.jpg","http://gm.cchtw.com/images/gm2.jpg","http://gm.cchtw.com/images/gm3.jpg","http://gm.cchtw.com/images/gm4.jpg","http://gm.cchtw.com/images/gm5.jpg","http://gm.cchtw.com/images/gm6.jpg","http://gm.cchtw.com/images/gm7.jpg","http://gm.cchtw.com/images/gm8.jpg","http://gm.cchtw.com/images/gm9.jpg","http://gm.cchtw.com/images/gm10.jpg","http://gm.cchtw.com/images/gm11.jpg","http://gm.cchtw.com/images/gm12.jpg","http://gm.cchtw.com/images/gm13.jpg","http://gm.cchtw.com/images/gm14.jpg","http://gm.cchtw.com/images/gm15.jpg","http://gm.cchtw.com/images/gm16.jpg"]}}
     */

    private int code;
    private String msg;
    private DataBean data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * cz_banner : {"url":"http://gm.cchtw.com/images/ic_banner1.png","data":["http://gm.cchtw.com/images/gm1.jpg","http://gm.cchtw.com/images/gm2.jpg","http://gm.cchtw.com/images/gm3.jpg","http://gm.cchtw.com/images/gm4.jpg","http://gm.cchtw.com/images/cz1.jpg","http://gm.cchtw.com/images/cz2.jpg","http://gm.cchtw.com/images/cz3.jpg","http://gm.cchtw.com/images/cz4.jpg","http://gm.cchtw.com/images/cz5.jpg","http://gm.cchtw.com/images/cz6.jpg","http://gm.cchtw.com/images/cz7.jpg","http://gm.cchtw.com/images/cz8.jpg","http://gm.cchtw.com/images/cz9.jpg","http://gm.cchtw.com/images/cz10.jpg","http://gm.cchtw.com/images/cz11.jpg","http://gm.cchtw.com/images/cz12.jpg"]}
         * gz_banner : {"url":"http://gm.cchtw.com/images/ic_banner2.png","data":["http://gm.cchtw.com/images/gm1.jpg","http://gm.cchtw.com/images/gm2.jpg","http://gm.cchtw.com/images/gm3.jpg","http://gm.cchtw.com/images/gm4.jpg","http://gm.cchtw.com/images/gm5.jpg","http://gm.cchtw.com/images/gm6.jpg","http://gm.cchtw.com/images/gm7.jpg","http://gm.cchtw.com/images/gm8.jpg","http://gm.cchtw.com/images/gm9.jpg","http://gm.cchtw.com/images/gm10.jpg","http://gm.cchtw.com/images/gm11.jpg","http://gm.cchtw.com/images/gm12.jpg","http://gm.cchtw.com/images/gm13.jpg","http://gm.cchtw.com/images/gm14.jpg","http://gm.cchtw.com/images/gm15.jpg","http://gm.cchtw.com/images/gm16.jpg"]}
         */

        private CzBannerBean cz_banner;
        private GzBannerBean gz_banner;

        public CzBannerBean getCz_banner() {
            return cz_banner;
        }

        public void setCz_banner(CzBannerBean cz_banner) {
            this.cz_banner = cz_banner;
        }

        public GzBannerBean getGz_banner() {
            return gz_banner;
        }

        public void setGz_banner(GzBannerBean gz_banner) {
            this.gz_banner = gz_banner;
        }

        public static class CzBannerBean {
            /**
             * url : http://gm.cchtw.com/images/ic_banner1.png
             * data : ["http://gm.cchtw.com/images/gm1.jpg","http://gm.cchtw.com/images/gm2.jpg","http://gm.cchtw.com/images/gm3.jpg","http://gm.cchtw.com/images/gm4.jpg","http://gm.cchtw.com/images/cz1.jpg","http://gm.cchtw.com/images/cz2.jpg","http://gm.cchtw.com/images/cz3.jpg","http://gm.cchtw.com/images/cz4.jpg","http://gm.cchtw.com/images/cz5.jpg","http://gm.cchtw.com/images/cz6.jpg","http://gm.cchtw.com/images/cz7.jpg","http://gm.cchtw.com/images/cz8.jpg","http://gm.cchtw.com/images/cz9.jpg","http://gm.cchtw.com/images/cz10.jpg","http://gm.cchtw.com/images/cz11.jpg","http://gm.cchtw.com/images/cz12.jpg"]
             */

            private String url;
            private List<String> data;

            public String getUrl() {
                return url;
            }

            public void setUrl(String url) {
                this.url = url;
            }

            public List<String> getData() {
                return data;
            }

            public void setData(List<String> data) {
                this.data = data;
            }
        }

        public static class GzBannerBean {
            /**
             * url : http://gm.cchtw.com/images/ic_banner2.png
             * data : ["http://gm.cchtw.com/images/gm1.jpg","http://gm.cchtw.com/images/gm2.jpg","http://gm.cchtw.com/images/gm3.jpg","http://gm.cchtw.com/images/gm4.jpg","http://gm.cchtw.com/images/gm5.jpg","http://gm.cchtw.com/images/gm6.jpg","http://gm.cchtw.com/images/gm7.jpg","http://gm.cchtw.com/images/gm8.jpg","http://gm.cchtw.com/images/gm9.jpg","http://gm.cchtw.com/images/gm10.jpg","http://gm.cchtw.com/images/gm11.jpg","http://gm.cchtw.com/images/gm12.jpg","http://gm.cchtw.com/images/gm13.jpg","http://gm.cchtw.com/images/gm14.jpg","http://gm.cchtw.com/images/gm15.jpg","http://gm.cchtw.com/images/gm16.jpg"]
             */

            private String url;
            private List<String> data;

            public String getUrl() {
                return url;
            }

            public void setUrl(String url) {
                this.url = url;
            }

            public List<String> getData() {
                return data;
            }

            public void setData(List<String> data) {
                this.data = data;
            }
        }
    }
}
