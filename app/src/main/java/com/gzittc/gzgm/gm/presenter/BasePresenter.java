package com.gzittc.gzgm.gm.presenter;

import android.os.Message;

import com.gzittc.gzgm.common.base.BaseView;
import com.gzittc.gzgm.gm.model.protocol.BaseProtocol;
import com.gzittc.gzgm.gm.model.protocol.CommonProtocol;

/**
 * Created by Administrator on 2017/11/18 0018.
 * MVP中所有Presenter的基类
 */

public class BasePresenter { //Activiyt，Fragment
    //mvp的View层
    protected BaseView baseView;
    //mvp的Model层
    protected CommonProtocol protocol;

    public BasePresenter(BaseView baseView) {
        this.baseView = baseView;
        protocol=new CommonProtocol();
    }

    //定义一个http访问的回调接口对象
    protected BaseProtocol.HttpCallback callback=new BaseProtocol.HttpCallback() {
        //访问http成功
        @Override
        public void onHttpSuccess(int reqType, Message msg) {
            //P层通过Model获取的数据， 通过回调把数据回传给View

            baseView.onHttpSuccess(reqType,msg);
        }
       //访问http失败
        @Override
        public void onHttpError( String error) {

            baseView.onHttpError(error);
        }
    };



}
