package com.gzittc.gzgm.gm.ui.view;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;

import com.gzittc.gzgm.R;

/**
 * Created by yzc on 2018-04-08.
 */

public class Mydialog extends Dialog {
    private Button buttonTure;
    private Button buttonFalse;
    private Spinner spinner;
    private int cur =0;

    private YesOnClickListener yesOnClickListener;
    private NoOnClickListener noOnClickListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog);
        setCanceledOnTouchOutside(false);
        initView();
        initData();
        initEvent();
    }

    private void initEvent() {
        buttonTure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(yesOnClickListener!=null){
                    yesOnClickListener.YesOnClick();
                }
            }
        });
        buttonFalse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(noOnClickListener!=null){
                    noOnClickListener.NoOnClick();
                }
            }
        });
    }

    private void initData() {

    }

    private void initView() {
        buttonTure = findViewById(R.id.yes);
        buttonFalse = findViewById(R.id.no);
        spinner = findViewById(R.id.spinner);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                setCur(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

    public Mydialog(Context context) {
        super(context, R.style.MyDialog);
    }

    public void setYesOnClickListener(YesOnClickListener yesOnClickListener){
        this.yesOnClickListener = yesOnClickListener;
    }
    public void setNoOnClickListener(NoOnClickListener noOnClickListener){
        this.noOnClickListener = noOnClickListener;
    }

    public void setCur(int cur) {
        this.cur = cur;
    }
    public int getCur(){
        return this.cur;
    }

    public interface YesOnClickListener {
        public void YesOnClick();
    }

    public interface NoOnClickListener {
        public void NoOnClick();
    }
}
