package com.gzittc.gzgm.gm.presenter;

import android.os.Message;

import com.gzittc.gzgm.common.base.BaseView;
import com.gzittc.gzgm.gm.model.bean.databean.MajorListBean;
import com.gzittc.gzgm.gm.model.protocol.BaseProtocol;
import com.gzittc.gzgm.gm.model.protocol.IHttpService;

import java.util.ArrayList;

/**
 * Created by 12236 on 2017/12/18.
 */

public class MajorListPresenter extends BasePresenter {


    public MajorListPresenter(BaseView baseView) {
        super(baseView);
    }
    //获取专业列表/
    public void getMajorList(String _param,int reqtype){
        protocol.getMajorList(_param, subCallback, reqtype);
    }

    BaseProtocol.HttpCallback subCallback=new BaseProtocol.HttpCallback() {
        @Override//通过请求接口数据解析
        public void onHttpSuccess(int reqType, Message msg) {
            if(reqType== IHttpService.GET_MAJOR_LIST){
                // 数据解析出来的数据，进行格式转换后再回调显示
                msg.obj = MajorLisrShopListData((MajorListBean) msg.obj);
                baseView.onHttpSuccess(reqType, msg);
                return;
            }
            baseView.onHttpSuccess(reqType,msg);
        }
        @Override
        public void onHttpError( String error) {
            subCallback.onHttpError(error);
        }
    };

    private ArrayList<MajorListBean.DataBean.SpecialtyDataBean> MajorLisrShopListData(MajorListBean obj) {
        ArrayList<MajorListBean.DataBean.SpecialtyDataBean> majorListBeen  = new ArrayList<>();
        majorListBeen.addAll(obj.getData().getSpecialty_data());
        return majorListBeen;
    }


}
