package com.gzittc.gzgm.gm.model.db.DBHelper;

import android.util.Log;

import com.gzittc.gzgm.common.base.Global;
import com.gzittc.gzgm.common.util.LogUtil;
import com.gzittc.gzgm.gm.model.db.bean.PSExam;

import org.litepal.crud.DataSupport;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

/**
 * Created by yzc on 2018-01-06.
 */

public class PSexamHelper {


    public PSexamHelper() {
        List<PSExam> mDestList = DataSupport.findAll(PSExam.class);
        if(mDestList == null){
            LogUtil.d("sql--","mDestList对象为空");
            return ;
        }
        if(mDestList.size() == 0){
            importSheet();
//            toString(DataSupport.findAll(PSExam.class));
        }else{
            LogUtil.d("sql--","已经有考试数据");
//            toString(DataSupport.findAll(PSExam.class));
        }
        //delete
//        DataSupport.deleteAll(PSExam.class);
    }

    private void toString(List<PSExam> datas) {
        for(PSExam bean : datas){
            Log.d("SQL_Test",": "+bean.getId()
                    +" "+bean.getType()
                    +" "+bean.getTitle()
                    +" "+bean.getA()
                    +" "+bean.getB()
                    +" "+bean.getC()
                    +" "+bean.getD()
                    +" "+bean.getAnswer() );
        }
    }

    private void importSheet() {
        try {
            InputStream is = Global.mContext.getResources().getAssets().open("17ps_exam.xls");
            Workbook book = Workbook.getWorkbook(is);
            Sheet sheet = book.getSheet(0);
            for (int j = 0; j < sheet.getRows(); ++j) {
                if(j==0){
                    continue;
                }
                if(sheet.getCell(0,j).getContents().equals("")){
                    break;
                }
                //读取到一行的每一列信息
                int id = Integer.parseInt(sheet.getCell(0,j).getContents());
                String type = sheet.getCell(1,j).getContents();
                String contents = sheet.getCell(2,j).getContents();
                String title = contents.substring(0,contents.indexOf("\n"));
                contents = contents.substring(contents.indexOf("\n")+1,contents.length());
                String A = contents.substring(0,contents.indexOf("\n"));
                contents = contents.substring(contents.indexOf("\n")+1,contents.length());
                String B = contents.substring(0,contents.indexOf("\n"));
                contents = contents.substring(contents.indexOf("\n")+1,contents.length());
                String C = contents.substring(0,contents.indexOf("\n"));
                contents = contents.substring(contents.indexOf("\n")+1,contents.length());
                String D = contents.substring(0,contents.length());
                String answer =sheet.getCell(3,j).getContents() ;
                //存入litepal操作
                PSExam psExam = new PSExam();
                psExam.setId( id );
                psExam.setType( type );
                psExam.setTitle( title );
                psExam.setA( A );
                psExam.setB( B );
                psExam.setC( C );
                psExam.setD( D );
                psExam.setAnswer( answer );
                psExam.save();
            }
            book.close();
        } catch (IOException | BiffException e) {
            e.printStackTrace();
        }
    }
}
