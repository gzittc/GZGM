package com.gzittc.gzgm.gm.model.bean.databean;

/**
 * Created by Administrator on 2017/12/15.
 */

public class CollegeSynopsisBean {

    /**
     * code : 0
     * msg : success
     * data : {"school_desc":"\n\n\n  \n  \n  \n  \n\n\n"}
     */

    private int code;
    private String msg;
    private DataBean data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * school_desc :
         */

        private String school_desc;

        public String getSchool_desc() {
            return school_desc;
        }

        public void setSchool_desc(String school_desc) {
            this.school_desc = school_desc;
        }
    }
}
