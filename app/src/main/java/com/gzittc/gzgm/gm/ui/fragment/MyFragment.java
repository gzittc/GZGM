package com.gzittc.gzgm.gm.ui.fragment;

import android.content.Intent;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gzittc.gzgm.R;
import com.gzittc.gzgm.common.base.BaseFragment;
import com.gzittc.gzgm.gm.model.db.bean.PSExam;
import com.gzittc.gzgm.gm.ui.activity.ExaminationPrepareActivity;
import com.gzittc.gzgm.gm.ui.activity.LoginActivity;
import com.gzittc.gzgm.gm.ui.view.Mydialog;

import org.litepal.crud.DataSupport;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Administrator on 2017/12/12.
 */

public class MyFragment extends BaseFragment {

    public static boolean isLogin = false;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_header)
    ImageView ivHeader;
    @BindView(R.id.tv_username)
    TextView tvUsername;
    @BindView(R.id.tv_stuNumber)
    TextView tvStuNumber;
    @BindView(R.id.ll_lession)
    LinearLayout llLession;
    @BindView(R.id.ll_shouchang)
    LinearLayout llShouchang;
    @BindView(R.id.ll_zhitiao)
    LinearLayout llZhitiao;
    @BindView(R.id.ll_shezhi)
    LinearLayout llShezhi;
    @BindView(R.id.student_message)
    LinearLayout studentMessage;
    @BindView(R.id.login_btn)
    Button loginBtn;
    @BindView(R.id.activity_main)
    FrameLayout activityMain;

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_my;
    }


    @Override
    public void initView() {
        ButterKnife.bind(this, mRoot);
        setPageTitle("我的");
    }

    @Override
    public void initListener() {

    }

    @Override
    public void initData() {

    }

    @Override
    public void onClick(View v, int id) {

    }

    @Override
    public void onHttpSuccess(int reqtype, Message msg) {

    }

    @Override
    public void onHttpError(String error) {

    }

    //判断是否登录
    public static boolean isLogin() {
        return isLogin;
    }


    private Intent intent = null;

    @OnClick({R.id.ll_exam,R.id.re_ll_login})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ll_exam:
                Exam_showdialog();
//                intent = new Intent(getActivity(), ExaminationPrepareActivity.class);
//                startActivity(intent);
                break;
            case R.id.re_ll_login:
                intent = new Intent(getActivity(), LoginActivity.class);
                startActivity(intent);
                break;
        }
    }
    private Mydialog mydialog;
    private void Exam_showdialog() {
        mydialog = new Mydialog(mActivity);
        mydialog.setYesOnClickListener(new Mydialog.YesOnClickListener() {
            @Override
            public void YesOnClick() {
                PrepareExam(mydialog.getCur());
                mydialog.dismiss();
            }
        });
        mydialog.setNoOnClickListener(new Mydialog.NoOnClickListener() {
            @Override
            public void NoOnClick() {
                mydialog.dismiss();
            }
        });
        mydialog.show();
    }
    private void PrepareExam(int cur) {
        intent = new Intent(getActivity(), ExaminationPrepareActivity.class);
        String sqlName = "";
        String subject = "";
        String examName = "";
        String time = "";
        String many = "";
        switch(cur){
            case 0:
                sqlName = "17ps_exam";
                subject = "计算机程序设计（移动互联网开发）";
                examName = "PS理论测试";
                time="30";
                many = DataSupport.findAll(PSExam.class).size()+"题";
                break;
            case 1:
                sqlName = "AndroidCase";
                subject = "计算机程序设计（移动互联网开发）";
                examName = "Android开发基础";
                time="30";
                many = "50题";
                break;
            default:
                break;
        }
        intent.putExtra("sql",sqlName);//哪个数据库
        intent.putExtra("subject",subject);
        intent.putExtra("content",examName);
        intent.putExtra("time",time);
        intent.putExtra("many", many);
        startActivity(intent);
    }
}
