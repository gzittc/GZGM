package com.gzittc.gzgm.gm.model.bean.databean;

import java.util.List;

/**
 * Created by Administrator on 2017/12/19.
 */

public class HomeQuestionBean {

    /**
     * code : 0
     * msg : success
     * data : {"question_list":[{"question_content":"测试添加问题","answer_content":"测试回答"}]}
     */

    private int code;
    private String msg;
    private DataBean data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        private List<QuestionListBean> question_list;

        public List<QuestionListBean> getQuestion_list() {
            return question_list;
        }

        public void setQuestion_list(List<QuestionListBean> question_list) {
            this.question_list = question_list;
        }

        public static class QuestionListBean {
            /**
             * question_content : 测试添加问题
             * answer_content : 测试回答
             */

            private String question_content;
            private String answer_content;

            public String getQuestion_content() {
                return question_content;
            }

            public void setQuestion_content(String question_content) {
                this.question_content = question_content;
            }

            public String getAnswer_content() {
                return answer_content;
            }

            public void setAnswer_content(String answer_content) {
                this.answer_content = answer_content;
            }
        }
    }
}
