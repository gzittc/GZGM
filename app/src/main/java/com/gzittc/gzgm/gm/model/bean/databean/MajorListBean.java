package com.gzittc.gzgm.gm.model.bean.databean;

import java.util.List;

/**
 * Created by Administrator on 2017/12/15.
 */

public class MajorListBean {

    /**
     * code : 0
     * msg : success
     * data : {"specialty_data":[{"specialty_id":"1","department_id":"4","specialty_name":"测试添加专业更新了...","specialty_logo":"http://gm.cchtw.com/uploads/20170523/64ae4d2d3e9aeea00e7c0091503096c7.jpg","specialty_year":"三年制"}]}
     */

    private int code;
    private String msg;
    private DataBean data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        private List<SpecialtyDataBean> specialty_data;

        public List<SpecialtyDataBean> getSpecialty_data() {
            return specialty_data;
        }

        public void setSpecialty_data(List<SpecialtyDataBean> specialty_data) {
            this.specialty_data = specialty_data;
        }

        public static class SpecialtyDataBean {
            /**
             * specialty_id : 1
             * department_id : 4
             * specialty_name : 测试添加专业更新了...
             * specialty_logo : http://gm.cchtw.com/uploads/20170523/64ae4d2d3e9aeea00e7c0091503096c7.jpg
             * specialty_year : 三年制
             */

            private String specialty_id;
            private String department_id;
            private String specialty_name;
            private String specialty_logo;
            private String specialty_year;

            public String getSpecialty_id() {
                return specialty_id;
            }

            public void setSpecialty_id(String specialty_id) {
                this.specialty_id = specialty_id;
            }

            public String getDepartment_id() {
                return department_id;
            }

            public void setDepartment_id(String department_id) {
                this.department_id = department_id;
            }

            public String getSpecialty_name() {
                return specialty_name;
            }

            public void setSpecialty_name(String specialty_name) {
                this.specialty_name = specialty_name;
            }

            public String getSpecialty_logo() {
                return specialty_logo;
            }

            public void setSpecialty_logo(String specialty_logo) {
                this.specialty_logo = specialty_logo;
            }

            public String getSpecialty_year() {
                return specialty_year;
            }

            public void setSpecialty_year(String specialty_year) {
                this.specialty_year = specialty_year;
            }
        }
    }
}
