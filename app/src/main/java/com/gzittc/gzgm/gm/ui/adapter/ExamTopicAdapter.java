package com.gzittc.gzgm.gm.ui.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

/**
 * Created by Administrator on 2017/12/22.
 */

public class ExamTopicAdapter extends FragmentPagerAdapter {

    List<Fragment> list;
    String[] tabTopics = null;

    public ExamTopicAdapter(FragmentManager fm, List<Fragment> list , String[] tabs) {
        super(fm);
        this.list = list;
        tabTopics = tabs;
    }

    @Override
    public Fragment getItem(int position) {
        return list == null ? null : list.get(position);
    }

    @Override
    public int getCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabTopics[position];
    }
}
