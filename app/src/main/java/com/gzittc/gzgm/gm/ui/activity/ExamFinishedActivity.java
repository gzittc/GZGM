package com.gzittc.gzgm.gm.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.gzittc.gzgm.R;
import com.gzittc.gzgm.common.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ExamFinishedActivity extends BaseActivity {

    @BindView(R.id.chengji)
    TextView chengji;
    @BindView(R.id.many)
    TextView many;
    @BindView(R.id.time)
    TextView time;
    @BindView(R.id.number)
    TextView number;

    @OnClick({R.id.btn_error, R.id.btn_home})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_error:
                break;
            case R.id.btn_home:
                finish();
                break;
        }
    }

    @Override
    public int getLayoutRes() {
        return R.layout.activity_exam_finished;
    }

    @Override
    public void initView() {
        ButterKnife.bind(this);
        Intent t = getIntent();
        chengji.setText(t.getStringExtra("chengji"));
        many.setText(t.getStringExtra("count"));
        time.setText(t.getStringExtra("time"));
        number.setText(t.getStringExtra("number"));
    }

    @Override
    public void initData() {

    }

    @Override
    public void onClick(View v, int id) {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }
}
