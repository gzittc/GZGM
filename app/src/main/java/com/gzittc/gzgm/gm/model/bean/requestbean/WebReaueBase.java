package com.gzittc.gzgm.gm.model.bean.requestbean;

/**
 * Created by Administrator on 2017/12/26.
 */

public class WebReaueBase {

    /**
     * _debug : 1
     * _access_sign : 232jgafdafdd1232
     * _access_timestamp : 152232
     */

    private String _debug = "1";//是否是调试模式
    private String _access_sign ="";//签名
    private String _access_timestamp="";//时间戳
    private String _c="";//接口类别
    private String _a="";//接口动作

    public String get_c() {
        return _c;
    }

    public void set_c(String _c) {
        this._c = _c;
    }

    public String get_a() {
        return _a;
    }

    public void set_a(String _a) {
        this._a = _a;
    }

    public String get_debug() {
        return _debug;
    }

    public void set_debug(String _debug) {
        this._debug = _debug;
    }

    public String get_access_sign() {
        return _access_sign;
    }

    public void set_access_sign(String _access_sign) {
        this._access_sign = _access_sign;
    }

    public String get_access_timestamp() {
        return _access_timestamp;
    }

    public void set_access_timestamp(String _access_timestamp) {
        this._access_timestamp = _access_timestamp;
    }
}
