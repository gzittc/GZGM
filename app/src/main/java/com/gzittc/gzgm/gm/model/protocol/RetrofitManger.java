package com.gzittc.gzgm.gm.model.protocol;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Retrofit单例
 * @author YZC
 * created at 2017-12-09
 */
public class RetrofitManger {
    private Retrofit retrofit;
    private IHttpService httpService;

    private static RetrofitManger instance = null;

    private RetrofitManger() {
        retrofit = new Retrofit
                .Builder()
                .baseUrl(httpService.HOST_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        httpService = retrofit.create(IHttpService.class);
    }


    public IHttpService getHttpService() {
        return httpService;
    }

    public Retrofit getRetrofit() {
        return retrofit;
    }

    public static RetrofitManger getInstance() {
        if(instance==null){
            synchronized (RetrofitManger.class){
                if(instance == null){
                    instance = new RetrofitManger();
                }
            }
        }
        return instance;
    }
}
