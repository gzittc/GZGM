package com.gzittc.gzgm.gm.ui.activity;

import android.graphics.Bitmap;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.gzittc.gzgm.R;
import com.gzittc.gzgm.common.base.BaseActivity;
import com.gzittc.gzgm.common.util.GsonUtils;
import com.gzittc.gzgm.gm.model.bean.databean.CollegeSceneryBean;
import com.gzittc.gzgm.gm.model.bean.databean.CollegeSynopsisBean;
import com.gzittc.gzgm.gm.model.bean.requestbean.WebReaueBase;
import com.gzittc.gzgm.gm.presenter.CollegeSynopsisPresenter;
import com.gzittc.gzgm.gm.ui.view.webview.X5WebView;
import com.tencent.smtt.export.external.interfaces.WebResourceResponse;
import com.tencent.smtt.sdk.WebChromeClient;
import com.tencent.smtt.sdk.WebView;
import com.tencent.smtt.sdk.WebViewClient;

import java.net.URL;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2017/12/21.
 */

public class WebViewActivity extends BaseActivity {
//    LinearLayout llIcRefresh;
//    @BindView(R.id.pb_web_loading)
//    ProgressBar pbWebLoading;
    @BindView(R.id.x5_wv_main)
    FrameLayout mViewParent;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.ll_ic_back)
    LinearLayout llBack;
    @BindView(R.id.iv_forward)
    ImageView ivForward;
    @BindView(R.id.ll_ic_forward)
    LinearLayout llForward;
    @BindView(R.id.iv_home)
    ImageView ivHome;
    @BindView(R.id.ll_ic_home)
    LinearLayout llHome;
    @BindView(R.id.iv_menu)
    ImageView ivMenu;
    @BindView(R.id.ll_ic_menu)
    LinearLayout llIcMenu;
    @BindView(R.id.bt_muti_windows)
    Button btMutiWindows;
    @BindView(R.id.ll_muti_windows)
    LinearLayout llMutiWindows;
    @BindView(R.id.ll_bottom)
    LinearLayout llBottom;
//    @BindView(R.id.pb_web_loading)
//    ProgressBar mPb;
//    @BindView(R.id.pb_web_loading)
//    ProgressBar mPb;

    private X5WebView mWebView;
    private WebViewClient mWebViewClient;
    private WebChromeClient mWebChromeClient;
    //    private ProgressBar mPb;
    private URL mIntentUrl;
    private String mURL_HOME_PAGE = "";
    private String webUrl;
    private boolean loadDone = true;
    private final float disable = 0.3f; //按钮半透明
    private final float enable = 1.0f; //按钮透明

    static final String mimeType = "text/html";
    static final String encoding = "utf-8";

    private CollegeSynopsisPresenter mCollegeSynopsisPresenter;

    private String id = "";
    private String id1;

    @Override
    public int getLayoutRes() {
        return R.layout.activity_web_view;
    }

    @Override
    public void initView() {
        ButterKnife.bind(this);
        mCollegeSynopsisPresenter = new CollegeSynopsisPresenter(this);
        initCollegeSynopsis();
        id = getIntent().getStringExtra("id");
        initWebView();

    }

    @Override
    public void initData() {
        if (mIntentUrl != null) {
            mWebView.loadUrl(mIntentUrl.toString());
        } else if (!TextUtils.isEmpty(mURL_HOME_PAGE)) {
            mWebView.loadUrl(mURL_HOME_PAGE);
        }
    }

    @Override
    public void onClick(View v, int id) {

    }

    private void initCollegeSynopsis() {
        WebReaueBase bean = new WebReaueBase();
        id1 = getIntent().getStringExtra("id");
        bean.set_a(id1);
        bean.set_c("config");
        String json = GsonUtils.objectToString(bean);
        if(id1.equals("schoolDesc")){
            mCollegeSynopsisPresenter.getCollegeSynopsis(json, 6);
        }else {
            mCollegeSynopsisPresenter.getCollegeScenery(json,7);
        }
    }
//   private void initCollegeScenery() {
//        WebReaueBase bean = new WebReaueBase();
//        bean.set_a(getIntent().getStringExtra("id"));
//        bean.set_c("config");
//
//        String json = GsonUtils.objectToString(bean);
//        mCollegeSceneryPresenter.getCollegeScenery(json, 7);
//
//    }

    @Override
    public void onHttpSuccess(int reqtype, Message msg) {
        if(id1.equals("schoolDesc")) {
            CollegeSynopsisBean bean = (CollegeSynopsisBean) msg.obj;
            mWebView.loadDataWithBaseURL(null, bean.getData().getSchool_desc(), mimeType, encoding, null);
        }else {
            CollegeSceneryBean bean = (CollegeSceneryBean) msg.obj;
            mWebView.loadDataWithBaseURL(null, bean.getData().getImages(), mimeType, encoding, null);
        }
//        String html = bean.getData().
//        mWebView.loadDataWithBaseURL(null, bean.getData().getSchool_desc(), mimeType, encoding, null);

    }

    private void initWebView() {
        mWebView = new X5WebView(this, null);
        mViewParent.addView(mWebView, new FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.MATCH_PARENT,
                FrameLayout.LayoutParams.MATCH_PARENT));
        X5WebView.setSmallWebViewEnabled(true);
        mWebViewClient = new MyWebViewClient();
        mWebChromeClient = new MyWebChromeClient();
        mWebView.setWebViewClient(mWebViewClient);
        mWebView.setWebChromeClient(mWebChromeClient);
    }

    private class MyWebChromeClient extends WebChromeClient {
        @Override
        public void onProgressChanged(WebView view, int progress) {
            super.onProgressChanged(view, progress);
//            if (progress >= 100) {
//                progress = 100;
//                mPb.setProgress(progress);
//                mPb.setVisibility(View.GONE);
//            } else {
//                if (progress >= 10) {
//                    mPb.setProgress(progress);
//                }
//                mPb.setVisibility(View.VISIBLE);
//            }
        }

        @Override
        public void onReceivedTitle(WebView arg0, String title) {
            super.onReceivedTitle(arg0, title);
            if ("School".equals(id)) {
                setPageTitle("学校简介");
            } else if ("xyfj".equals(id)) {
                setPageTitle("学院风景");
            }

        }

    }

    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            return false;
        }

        @Override
        public WebResourceResponse shouldInterceptRequest(WebView arg0, String arg1) {
            return super.shouldInterceptRequest(arg0, arg1);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap arg2) {
            super.onPageStarted(view, url, arg2);
            webUrl = url;
            loadDone = false;
//            ivRefresh.setBackgroundResource(R.drawable.ic_stop);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            webUrl = url;
            loadDone = true;
//            ivRefresh.setBackgroundResource(R.drawable.ic_refersh);
            changeBackForwardButton(view);
        }
    }

    private void changeBackForwardButton(WebView webView) {
        if (webView.canGoBack()) {
            ivBack.setAlpha(enable);
            llBack.setEnabled(true);
        } else {
            ivBack.setAlpha(disable);
            llBack.setEnabled(false);
        }
        if (webView.canGoForward()) {
            ivForward.setAlpha(enable);
            llForward.setEnabled(true);
        } else {
            ivForward.setAlpha(disable);
            llForward.setEnabled(false);
        }
        if (webView.getUrl() != null && webView.getUrl().equalsIgnoreCase(mURL_HOME_PAGE)) {
            ivHome.setAlpha(disable);
            llHome.setEnabled(false);
        } else {
            ivHome.setAlpha(enable);
            llHome.setEnabled(true);
        }
    }

}


