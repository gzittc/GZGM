package com.gzittc.gzgm.gm.model.db.bean;

import org.litepal.crud.DataSupport;

/**
 * 判断题
 * Created by yzc on 2017-12-15.
 */

public class PDSubject extends DataSupport {
    private int id;
    private String main_title;//题目的内容
    private String a;//A答案=对
    private String b;//B答案=错
    private String answer;//题目的正确答案---指向A or B
    private int main_title_len;//题目的长度（字符总数）

    public String getMain_title() {
        return main_title;
    }

    public void setMain_title(String main_title) {
        this.main_title = main_title;
    }

    public String getA() {
        return a;
    }

    public void setA(String a) {
        this.a = a;
    }

    public String getB() {
        return b;
    }

    public void setB(String b) {
        this.b = b;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public int getMain_title_len() {
        return main_title_len;
    }

    public void setMain_title_len(int main_title_len) {
        this.main_title_len = main_title_len;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
