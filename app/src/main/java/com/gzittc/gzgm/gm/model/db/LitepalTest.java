package com.gzittc.gzgm.gm.model.db;

import android.content.ContentValues;
import android.util.Log;

import com.gzittc.gzgm.gm.model.db.bean.PDSubject;

import org.litepal.crud.DataSupport;

import java.util.List;

import static org.litepal.crud.DataSupport.findAll;
import static org.litepal.crud.DataSupport.updateAll;

/**
 * Created by yzc on 2017-12-15.
 */

public class LitepalTest {

    private final String SQL_Test = "SQL_Test";
    public LitepalTest( ) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Log.d(SQL_Test,": ------------插入数据------------");
                PDSubject pdSubject = null;
                for(int i=1;i<=2;i++){
                    pdSubject = new PDSubject();
//                    pdSubject.setRec_no(i);
//                    pdSubject.setPd_test_id(i);
                    pdSubject.setMain_title("这里是第"+i+"题");
                    pdSubject.setA("对");
                    pdSubject.setB("错");
                    pdSubject.setAnswer("a");
                    pdSubject.setMain_title_len(10);
                    Log.d(SQL_Test,": 插入数据-"+pdSubject.save());
                }
                Log.d("SQL_Test",": ------------插入数据------------");

                Log.d(SQL_Test,": --------查询数据------------");
                List<PDSubject> datas = findAll(PDSubject.class);
                toStirng(datas);
                Log.d(SQL_Test,": ------------查询数据------------");

                Log.d(SQL_Test,": ------------更改数据------------");
                ContentValues values = new ContentValues();
                values.put("main_title_len",1000);
                updateAll(PDSubject.class, values, "pd_test_id = ?", "2");
                List<PDSubject> datas1 = findAll(PDSubject.class);
                toStirng(datas1);
                Log.d(SQL_Test,": ------------更改数据------------");

                Log.d(SQL_Test,": ------------删除数据------------");
                DataSupport.deleteAll(PDSubject.class);
                List<PDSubject> datas2 = DataSupport.findAll(PDSubject.class);
                Log.d(SQL_Test,": "+(datas2==null?0:datas2.size() ) );
                Log.d(SQL_Test,": ------------删除数据------------");
            }
        }).start();

    }

    private void toStirng(List<PDSubject> datas) {
        for(PDSubject pdSubject : datas){
            Log.d(SQL_Test,": "+pdSubject.getId()
//                    +" "+pdSubject.getRec_no()
//                    +" "+pdSubject.getPd_test_id()
                    +" "+pdSubject.getMain_title()
                    +" "+pdSubject.getA()
                    +" "+pdSubject.getB()
                    +" "+pdSubject.getAnswer()
                    +" "+pdSubject.getMain_title_len());
        }
    }
}
