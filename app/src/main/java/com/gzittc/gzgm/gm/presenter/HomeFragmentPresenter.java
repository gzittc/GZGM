package com.gzittc.gzgm.gm.presenter;

import android.os.Message;

import com.gzittc.gzgm.common.base.BaseView;
import com.gzittc.gzgm.gm.model.bean.databean.HomeBannerBean;
import com.gzittc.gzgm.gm.model.bean.databean.HomeQuestionBean;
import com.gzittc.gzgm.gm.model.protocol.BaseProtocol;
import com.gzittc.gzgm.gm.model.protocol.IHttpService;

import java.util.ArrayList;

/**
 * Created by Administrator on 2017/12/18.
 */

public class HomeFragmentPresenter extends BasePresenter {



    public HomeFragmentPresenter(BaseView baseView) {
        super(baseView);
    }

    //获取首页banner
    public void getHomeBanner(String _param,int reqtype){
        protocol.getHomeBanner(_param, subCallback, reqtype);
    }

    public void getHomeQuestion(String _param,int reqtype){
        protocol.getHomeQuestion(_param, subCallback, reqtype);
    }

    //子类自定义CallBack，先转换数据，再回调,数据处理放在P层，View层只做数据展示
    BaseProtocol.HttpCallback subCallback=new BaseProtocol.HttpCallback() {
        @Override
        public void onHttpSuccess(int reqType, Message msg) {
            if(reqType== IHttpService.GET_HOME_BANNER){
                // 数据解析出来的数据，进行格式转换后再回调显示
                msg.obj = transformBannerData((HomeBannerBean) msg.obj);
                baseView.onHttpSuccess(reqType, msg);//
                return;
            }
            if (reqType== IHttpService.GET_HOME_QUESTION){
//                HomeQuestionBean homeQuestionBean = (HomeQuestionBean) msg.obj;
                msg.obj = transformQuestionData((HomeQuestionBean) msg.obj);
                baseView.onHttpSuccess(reqType,msg);
            }
            baseView.onHttpSuccess(reqType,msg);
        }
        @Override
        public void onHttpError( String error) {
            baseView.onHttpError(error);
        }
    };

    private ArrayList<String> transformBannerData(HomeBannerBean obj) {
        ArrayList pageData = new ArrayList();
        pageData.add(obj.getData().getCz_banner().getUrl());
        pageData.add(obj.getData().getGz_banner().getUrl());
        return pageData;
    }
    private ArrayList<String> transformQuestionData(HomeQuestionBean obj) {
        ArrayList pageData = new ArrayList();
//        Arrayz questionListBean = (HomeQuestionBean.DataBean.QuestionListBean) obj.getData().getQuestion_list();
        pageData.addAll(obj.getData().getQuestion_list());
        return pageData;
    }
}
