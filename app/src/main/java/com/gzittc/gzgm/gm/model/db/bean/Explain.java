package com.gzittc.gzgm.gm.model.db.bean;

import org.litepal.crud.DataSupport;

/**
 * Created by yzc on 2017-12-16.
 */

public class Explain extends DataSupport {
    private int explain_id;//>对应题目id
    private String explain;//题目解释
    private int image_id;//没有图片图片id地址 -2位没有
    private String table_name;//表名
    private int table_id;//在表中的id

    public int getExplain_id() {
        return explain_id;
    }

    public void setExplain_id(int explain_id) {
        this.explain_id = explain_id;
    }

    public String getExplain() {
        return explain;
    }

    public void setExplain(String explain) {
        this.explain = explain;
    }

    public int getImage_id() {
        return image_id;
    }

    public void setImage_id(int image_id) {
        this.image_id = image_id;
    }

    public String getTable_name() {
        return table_name;
    }

    public void setTable_name(String table_name) {
        this.table_name = table_name;
    }

    public int getTable_id() {
        return table_id;
    }

    public void setTable_id(int table_id) {
        this.table_id = table_id;
    }
}
