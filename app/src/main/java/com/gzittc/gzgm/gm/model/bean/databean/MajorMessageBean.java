package com.gzittc.gzgm.gm.model.bean.databean;

/**
 * Created by Administrator on 2017/12/15.
 */

public class MajorMessageBean {

    /**
     * code : 0
     * msg : success
     * data : {"specialty_info":{"specialty_id":"1","department_id":"4","specialty_name":"测试添加专业更新了...","specialty_logo":"/uploads/20170523/64ae4d2d3e9aeea00e7c0091503096c7.jpg","specialty_desc":"测试添加专业更新了...","specialty_year":"三年","specialty_target":"高中毕业生","specialty_fee":"测试添加专业更新了...","specialty_course":"测试添加专业更新了...","specialty_certificate":"测试添加专业更新了...","specialty_employment":"测试添加专业更新了...","specialty_prizes":"测试添加专业更新了..."}}
     */

    private int code;
    private String msg;
    private DataBean data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * specialty_info : {"specialty_id":"1","department_id":"4","specialty_name":"测试添加专业更新了...","specialty_logo":"/uploads/20170523/64ae4d2d3e9aeea00e7c0091503096c7.jpg","specialty_desc":"测试添加专业更新了...","specialty_year":"三年","specialty_target":"高中毕业生","specialty_fee":"测试添加专业更新了...","specialty_course":"测试添加专业更新了...","specialty_certificate":"测试添加专业更新了...","specialty_employment":"测试添加专业更新了...","specialty_prizes":"测试添加专业更新了..."}
         */

        private SpecialtyInfoBean specialty_info;

        public SpecialtyInfoBean getSpecialty_info() {
            return specialty_info;
        }

        public void setSpecialty_info(SpecialtyInfoBean specialty_info) {
            this.specialty_info = specialty_info;
        }

        public static class SpecialtyInfoBean {
            /**
             * specialty_id : 1
             * department_id : 4
             * specialty_name : 测试添加专业更新了...
             * specialty_logo : /uploads/20170523/64ae4d2d3e9aeea00e7c0091503096c7.jpg
             * specialty_desc : 测试添加专业更新了...
             * specialty_year : 三年
             * specialty_target : 高中毕业生
             * specialty_fee : 测试添加专业更新了...
             * specialty_course : 测试添加专业更新了...
             * specialty_certificate : 测试添加专业更新了...
             * specialty_employment : 测试添加专业更新了...
             * specialty_prizes : 测试添加专业更新了...
             */

            private String specialty_id;
            private String department_id;
            private String specialty_name;
            private String specialty_logo;
            private String specialty_desc;
            private String specialty_year;
            private String specialty_target;
            private String specialty_fee;
            private String specialty_course;
            private String specialty_certificate;
            private String specialty_employment;
            private String specialty_prizes;

            public String getSpecialty_id() {
                return specialty_id;
            }

            public void setSpecialty_id(String specialty_id) {
                this.specialty_id = specialty_id;
            }

            public String getDepartment_id() {
                return department_id;
            }

            public void setDepartment_id(String department_id) {
                this.department_id = department_id;
            }

            public String getSpecialty_name() {
                return specialty_name;
            }

            public void setSpecialty_name(String specialty_name) {
                this.specialty_name = specialty_name;
            }

            public String getSpecialty_logo() {
                return specialty_logo;
            }

            public void setSpecialty_logo(String specialty_logo) {
                this.specialty_logo = specialty_logo;
            }

            public String getSpecialty_desc() {
                return specialty_desc;
            }

            public void setSpecialty_desc(String specialty_desc) {
                this.specialty_desc = specialty_desc;
            }

            public String getSpecialty_year() {
                return specialty_year;
            }

            public void setSpecialty_year(String specialty_year) {
                this.specialty_year = specialty_year;
            }

            public String getSpecialty_target() {
                return specialty_target;
            }

            public void setSpecialty_target(String specialty_target) {
                this.specialty_target = specialty_target;
            }

            public String getSpecialty_fee() {
                return specialty_fee;
            }

            public void setSpecialty_fee(String specialty_fee) {
                this.specialty_fee = specialty_fee;
            }

            public String getSpecialty_course() {
                return specialty_course;
            }

            public void setSpecialty_course(String specialty_course) {
                this.specialty_course = specialty_course;
            }

            public String getSpecialty_certificate() {
                return specialty_certificate;
            }

            public void setSpecialty_certificate(String specialty_certificate) {
                this.specialty_certificate = specialty_certificate;
            }

            public String getSpecialty_employment() {
                return specialty_employment;
            }

            public void setSpecialty_employment(String specialty_employment) {
                this.specialty_employment = specialty_employment;
            }

            public String getSpecialty_prizes() {
                return specialty_prizes;
            }

            public void setSpecialty_prizes(String specialty_prizes) {
                this.specialty_prizes = specialty_prizes;
            }
        }
    }
}
