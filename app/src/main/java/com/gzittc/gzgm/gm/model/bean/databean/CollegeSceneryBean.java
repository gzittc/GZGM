package com.gzittc.gzgm.gm.model.bean.databean;

/**
 * Created by Administrator on 2017/12/15.
 */

public class CollegeSceneryBean {

    /**
     * code : 0
     * msg : success
     * data : {"images":"\n\n\n  \n  \n  \n  \n\n"}
     */

    private int code;
    private String msg;
    private DataBean data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * images :
         */

        private String images;

        public String getImages() {
            return images;
        }

        public void setImages(String images) {
            this.images = images;
        }
    }
}
