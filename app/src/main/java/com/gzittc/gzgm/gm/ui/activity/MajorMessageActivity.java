package com.gzittc.gzgm.gm.ui.activity;

import android.os.Message;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.gzittc.gzgm.R;
import com.gzittc.gzgm.common.base.BaseActivity;
import com.gzittc.gzgm.common.util.GsonUtils;
import com.gzittc.gzgm.gm.model.bean.databean.MajorMessageBean;
import com.gzittc.gzgm.gm.model.bean.requestbean.MajorMessageRequestBean;
import com.gzittc.gzgm.gm.presenter.MajorMessagePresenter;
import com.gzittc.gzgm.gm.ui.view.textview.HtmlTextView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by 12236 on 2017/12/19.
 */

public class MajorMessageActivity extends BaseActivity {


    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.tv_left)
    TextView tvLeft;
    @BindView(R.id.tv_title)
    TextView tvToolbarTitle;
    @BindView(R.id.tv_right)
    TextView tvRight;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tv_duixiang)
    TextView tvDuixiang;
    @BindView(R.id.tv_nianzhi)
    TextView tvNianzhi;
    @BindView(R.id.tv_xuezafei)
    TextView tvXuezafei;
    @BindView(R.id.tv_zhutikecheng)
    TextView tvZhutikecheng;
    @BindView(R.id.tv_kaoquzhengshu)
    TextView tvKaoquzhengshu;
    @BindView(R.id.tv_jiuyefangxiang)
    TextView tvJiuyefangxiang;
    @BindView(R.id.tv_huoqujiangxiang)
    TextView tvHuoqujiangxiang;
    @BindView(R.id.tv_main)
    HtmlTextView tvMain;
    private MajorMessagePresenter mMajorMessagePresenter;

    @Override
    public int getLayoutRes() {
        return R.layout.activity_major_details;
    }

    @Override
    public void initView() {
        ButterKnife.bind(this);
        mMajorMessagePresenter = new MajorMessagePresenter(this);
        initMajorMessage();
    }

    @Override
    public void initListener() {
        super.initListener();
    }

    @Override
    public void initData() {

    }

    @Override
    public void onClick(View v, int id) {

    }

    private void initMajorMessage() {
        MajorMessageRequestBean bean = new MajorMessageRequestBean();
        bean._a = "read";
        bean._c = "specialty";
        String sid = getIntent().getStringExtra("specialty_id");
        bean.specialty_id = sid;
        String json = GsonUtils.objectToString(bean);
        mMajorMessagePresenter.getMajorMessage(json, 5);
    }

    //访问网络数据
    @Override
    public void onHttpSuccess(int reqtype, Message msg) {
        MajorMessageBean m = (MajorMessageBean) msg.obj;
        tvToolbarTitle.setText(m.getData().getSpecialty_info().getSpecialty_name());
        tvMain.setHtmlFromString(m.getData().getSpecialty_info().getSpecialty_desc(),false);
        tvDuixiang.setText(m.getData().getSpecialty_info().getSpecialty_target());
        tvHuoqujiangxiang.setText(m.getData().getSpecialty_info().getSpecialty_prizes());
        tvJiuyefangxiang.setText(m.getData().getSpecialty_info().getSpecialty_employment());
        tvKaoquzhengshu.setText(m.getData().getSpecialty_info().getSpecialty_certificate());
        tvNianzhi.setText(m.getData().getSpecialty_info().getSpecialty_year());
        tvXuezafei.setText(m.getData().getSpecialty_info().getSpecialty_fee());
        tvZhutikecheng.setText(m.getData().getSpecialty_info().getSpecialty_course());

    }

    @Override
    public void onHttpError(String error) {
        super.onHttpError(error);
    }




//    @OnClick(R.id.img_back)
//    public void onViewClicked() {
//        finish();
//    }
}
