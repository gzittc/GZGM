package com.gzittc.gzgm.gm.ui.fragment;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.gzittc.gzgm.R;
import com.gzittc.gzgm.common.base.BaseFragment;
import com.gzittc.gzgm.gm.model.db.bean.Case.Choice;
import com.gzittc.gzgm.gm.model.db.bean.PSExam;
import com.gzittc.gzgm.gm.ui.activity.ExamStartActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Administrator on 2017/12/22.
 */

public class ExamTopicFragment extends BaseFragment {
    @BindView(R.id.id)
    TextView titleId;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.a_answer)
    Button aAnswer;
    @BindView(R.id.b_answer)
    Button bAnswer;
    @BindView(R.id.c_answer)
    Button cAnswer;
    @BindView(R.id.d_answer)
    Button dAnswer;

    final static String PDTEST_TYPE = "判断题";//判断
    final static String DXTEST_TYPE = "单选题";//单选
    final static String DXXTEST_TYPE = "多选题";//多选
    String indextype = "";//当前类型题目

    private String studentAnswer = "";
    ExamStartActivity examStartActivity = null;
    PSExam bean;
    int TYPE =0;
    Choice choice;

    /**
     * 查询数据库后给fragment设置题目答案的bean
     */
    public void setContent(PSExam bean) {
        this.bean = bean;
    }

    public void setContent(int type,Choice choices){
        this.choice = choices;
        TYPE = type;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_exam_topic;
    }

    @Override
    public void initView() {
        ButterKnife.bind(this, mRoot);
        if(TYPE == 1){
            titleId.setText(""+((choice.getPart()-1)*5+choice.getNum() ) );
            title.setText(choice.getTitle());
            aAnswer.setText(choice.getA());
            bAnswer.setText(choice.getB());
            cAnswer.setText(choice.getC());
            dAnswer.setText(choice.getD());
            indextype = DXTEST_TYPE;
        }else{
            titleId.setText(""+bean.getId());
            title.setText(bean.getTitle());
            aAnswer.setText(bean.getA());
            bAnswer.setText(bean.getB());
            cAnswer.setText(bean.getC());
            dAnswer.setText(bean.getD());
            switch(bean.getType()){
                case PDTEST_TYPE://判断
                    indextype = PDTEST_TYPE;
                    //只有两个答案
                    cAnswer.setVisibility(View.GONE);
                    dAnswer.setVisibility(View.GONE);
                    break;
                case DXTEST_TYPE://单选
                    indextype = DXTEST_TYPE;
                    break;
                case DXXTEST_TYPE://多选
                    indextype = DXXTEST_TYPE;
                    break;
            }
        }
        examStartActivity = ((ExamStartActivity)mActivity);

    }

    @Override
    public void initListener() {

    }

    @Override
    public void initData() {
    }

    @Override
    public void onClick(View v, int id) {

    }

    @OnClick({R.id.a_answer, R.id.b_answer,R.id.c_answer,R.id.d_answer})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.a_answer:
                FlashView(0);
                break;
            case R.id.b_answer:
                FlashView(1);
                break;
            case R.id.c_answer:
                FlashView(2);
                break;
            case R.id.d_answer:
                FlashView(3);
                break;
        }
    }
    /**
     * 按钮显示逻辑
     * @author YZC
     * created at 2018-01-07
     */
    boolean a=false;
    boolean b=false;
    boolean c=false;
    boolean d=false;
    boolean[] temp_answer = new boolean[4];
    private void FlashView(int which) {
        if(indextype == PDTEST_TYPE){//判断
            isSelect(which);//按钮背景逻辑
            temp_answer = new boolean[4];//初始化答案
            temp_answer[which]=true;//选择答案
//            examStartActivity.setAnaswer(temp_answer,bean.getId());//提交答案到activity
            examStartActivity.NextFragment();//选择完，跳到下一题
        }else if(indextype == DXTEST_TYPE){//单选
            isSelect(which);//按钮背景逻辑
            temp_answer = new boolean[4];//初始化答案
            temp_answer[which]=true;//选择答案
//            examStartActivity.setAnaswer(temp_answer,bean.getId());//提交答案到activity
            examStartActivity.NextFragment();//选择完，跳到下一题
        }else{//多选
            switch (which){//按钮背景逻辑
                case 0:
                    aAnswer.setSelected(a?false:true);
                    temp_answer[0]=!temp_answer[0];
                    break;
                case 1:
                    bAnswer.setSelected(a?false:true);
                    temp_answer[1]=!temp_answer[1];
                    break;
                case 2:
                    cAnswer.setSelected(a?false:true);
                    temp_answer[2]=!temp_answer[2];
                    break;
                case 3:
                    dAnswer.setSelected(a?false:true);
                    temp_answer[3]=!temp_answer[3];
                    break;
            }
        }
        if(TYPE == 1){
            examStartActivity.setAnaswer(temp_answer,((choice.getPart()-1)*5+choice.getNum() ) );//提交答案
        }else {
            examStartActivity.setAnaswer(temp_answer, bean.getId());//提交答案
        }
    }

    /**
     * 按钮背景逻辑
     * @author YZC
     * created at 2018-01-07
     */
    public void isSelect(int which) {
        aAnswer.setSelected(false);
        bAnswer.setSelected(false);
        cAnswer.setSelected(false);
        dAnswer.setSelected(false);
        switch(which){
            case 0:
                aAnswer.setSelected(true);
                break;
            case 1:
                bAnswer.setSelected(true);
                break;
            case 2:
                cAnswer.setSelected(true);
                break;
            case 3:
                dAnswer.setSelected(true);
                break;
        }
    }

}
