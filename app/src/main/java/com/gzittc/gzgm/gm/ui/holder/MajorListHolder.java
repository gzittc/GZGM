package com.gzittc.gzgm.gm.ui.holder;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.gzittc.gzgm.R;
import com.gzittc.gzgm.common.ui.BaseAdapterLV;
import com.gzittc.gzgm.common.ui.BaseHolderLV;
import com.gzittc.gzgm.gm.model.bean.databean.MajorListBean;
import com.gzittc.gzgm.gm.model.bean.databean.MajorMessageBean;
import com.gzittc.gzgm.gm.ui.activity.MajorMessageActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;

/**
 * Created by 12236 on 2017/12/18.
 */

public class MajorListHolder extends BaseHolderLV<MajorListBean.DataBean.SpecialtyDataBean> implements View.OnClickListener{

    @BindView(R.id.iv_image)
    ImageView ivImage;
    @BindView(R.id.tv_major_list_name)
    TextView tvMajorListName;
    @BindView(R.id.tv_year)
    TextView tvYear;
    private Context context;
    private ArrayList<MajorMessageBean> MajorListBeanArrayList = null;

    private int position;
    private MajorListBean.DataBean.SpecialtyDataBean bean;


    public MajorListHolder(Context context, ViewGroup parent, BaseAdapterLV adapter, int position, MajorListBean.DataBean.SpecialtyDataBean bean) {
        super(context, parent, adapter, position, bean);
        this.context = context;
        this.position = position;
        this.bean = bean;
    }

    @Override
    public View onCreateView(Context context, ViewGroup parent) {

       // View v = parent.findViewById(R.layout.item_major_list);
          View view = inflater.inflate(R.layout.item_major_list,parent,false);

          view.setOnClickListener(this);
          ivImage = view.findViewById(R.id.iv_image);
          tvMajorListName = view.findViewById(R.id.tv_major_list_name);
          tvYear = view.findViewById(R.id.tv_year);

         return view;

    }

    @Override
    protected void onRefreshView(MajorListBean.DataBean.SpecialtyDataBean bean, int position) {
        Picasso.with(context).load(bean.getSpecialty_logo()).into(ivImage);
        tvYear.setText(bean.getSpecialty_year());
        tvMajorListName.setText(bean.getSpecialty_name());
    }


    @Override
    public void onClick(View v) {
        Intent intent = new Intent(context, MajorMessageActivity.class);
        MajorListBeanArrayList =new ArrayList<MajorMessageBean>();
        //intent.putExtra("specialty_id",position+1+"");
        intent.putExtra("specialty_id",bean.getSpecialty_id());
        context.startActivity(intent);
    }

}
