package com.gzittc.gzgm.gm.model.db.DBHelper;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.gzittc.gzgm.gm.model.db.bean.Case.Choice;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import static com.gzittc.gzgm.common.base.Global.mContext;

/**
 * Created by yzc on 2018-04-08.
 */

public class AndroidCaseManger {
    private String DB_NAME = "AndroidCase.db";
    private Context context;
    private String packName="com.gzittc.gzgm";

    public AndroidCaseManger(Context context) {
        this.context = context;
    }
    //把assets目录下的db文件复制到dbpath下
    public SQLiteDatabase DBManager() {
        String dbPath = "/data/data/" + packName
                + "/databases/" + DB_NAME;
        if (!new File(dbPath).exists()) {
            try {
                FileOutputStream out = new FileOutputStream(dbPath);
                InputStream in = mContext.getAssets().open(DB_NAME);
                byte[] buffer = new byte[1024];
                int readBytes = 0;
                while ((readBytes = in.read(buffer)) != -1)
                    out.write(buffer, 0, readBytes);
                in.close();
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return SQLiteDatabase.openOrCreateDatabase(dbPath, null);
    }
    //查询
    public void query(SQLiteDatabase sqliteDB, String[] columns, String selection, String[] selectionArgs) {
        try {
            String table = "part";
            Cursor cursor = sqliteDB.query(table, columns, selection, selectionArgs, null, null, null);
            while (cursor.moveToNext()) {
                Log.d("MainActivity",cursor.getString(cursor.getColumnIndex("type")));
                Log.d("MainActivity",cursor.getString(cursor.getColumnIndex("part")));
                Log.d("MainActivity",cursor.getString(cursor.getColumnIndex("partname")));
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Choice> queryChioceAll(SQLiteDatabase DB){
        ArrayList<Choice> list = new ArrayList<>();
        try {
            String table = "choice";
            Cursor cursor = DB.query(table, new String[]{"*"},null , null, null, null, null);
            while (cursor.moveToNext()) {
                list.add(new Choice(
                        cursor.getInt(cursor.getColumnIndex("type")),
                        cursor.getInt(cursor.getColumnIndex("num")),
                        cursor.getString(cursor.getColumnIndex("title")),
                        cursor.getString(cursor.getColumnIndex("A")),
                        cursor.getString(cursor.getColumnIndex("B")),
                        cursor.getString(cursor.getColumnIndex("C")),
                        cursor.getString(cursor.getColumnIndex("D")),
                        cursor.getString(cursor.getColumnIndex("answer")),
                        cursor.getInt(cursor.getColumnIndex("part"))
                ));
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public void queryJudgeAll(SQLiteDatabase DB, String[] columns, String selection, String[] selectionArgs){

    }
}
