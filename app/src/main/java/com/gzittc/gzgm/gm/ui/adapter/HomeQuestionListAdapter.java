package com.gzittc.gzgm.gm.ui.adapter;

import android.content.Context;
import android.view.ViewGroup;

import com.gzittc.gzgm.common.ui.BaseAdapterLV;
import com.gzittc.gzgm.common.ui.BaseHolderLV;
import com.gzittc.gzgm.gm.model.bean.databean.HomeQuestionBean;
import com.gzittc.gzgm.gm.ui.holder.QuestionViewHolder;

import java.util.List;

/**
 * Created by Administrator on 2017/12/19.
 */

public class HomeQuestionListAdapter extends BaseAdapterLV<HomeQuestionBean.DataBean.QuestionListBean> {
    public HomeQuestionListAdapter(Context context, List listData) {
        super(context, listData);
    }

    @Override
    public BaseHolderLV createViewHolder(Context context, ViewGroup parent, HomeQuestionBean.DataBean.QuestionListBean bean, int position) {
        return new QuestionViewHolder(context,parent,this,position,bean);
    }


}
