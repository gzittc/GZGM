package com.gzittc.gzgm.gm.model.db.bean;

import org.litepal.crud.DataSupport;

/**
 * 简答题
 * Created by yzc on 2017-12-16.
 */

public class JDSubject extends DataSupport {
    private int subject_id;//题目在本表的序号
    private String main_title;//题目的内容
    private String answer;//题目的正确答案
    private int main_title_len;//题目的长度

    public int getSubject_id() {
        return subject_id;
    }

    public void setSubject_id(int subject_id) {
        this.subject_id = subject_id;
    }

    public String getMain_title() {
        return main_title;
    }

    public void setMain_title(String main_title) {
        this.main_title = main_title;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public int getMain_title_len() {
        return main_title_len;
    }

    public void setMain_title_len(int main_title_len) {
        this.main_title_len = main_title_len;
    }
}
