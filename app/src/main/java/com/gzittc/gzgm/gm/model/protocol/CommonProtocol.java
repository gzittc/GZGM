package com.gzittc.gzgm.gm.model.protocol;

import com.gzittc.gzgm.gm.model.bean.databean.CollegeSceneryBean;
import com.gzittc.gzgm.gm.model.bean.databean.CollegeSynopsisBean;
import com.gzittc.gzgm.gm.model.bean.databean.HomeBannerBean;
import com.gzittc.gzgm.gm.model.bean.databean.HomeQuestionBean;
import com.gzittc.gzgm.gm.model.bean.databean.MajorListBean;
import com.gzittc.gzgm.gm.model.bean.databean.MajorMessageBean;

/**
 * 通用请求类
 * @author YZC
 * created at 2017-12-09
 */
public class CommonProtocol extends BaseProtocol {

    public void getHomeBanner(String _param, final HttpCallback callback, int reqtype){
        super.execute(super.getHttpService().getHomeBanner(_param),callback, HomeBannerBean.class,reqtype);
    }

    public void getMajorList(String _param, final HttpCallback callback, int reqtype) {
        super.execute(super.getHttpService().getMajorList(_param), callback, MajorListBean.class, reqtype);
    }
    public void getHomeQuestion(String _param, final HttpCallback callback, int reqtype){
        super.execute(super.getHttpService().getHomeQuestion(_param),callback, HomeQuestionBean.class,reqtype);
    }
    public void getMajorMessage(String _param, final HttpCallback callback, int reqtype){
        super.execute(super.getHttpService().getMajorMessage(_param),callback, MajorMessageBean.class,reqtype);
    }
    public void getCollegeSynopsis(String _param, final HttpCallback callback, int reqtype){
        super.execute(super.getHttpService().getCollegeSynopsis(_param),callback, CollegeSynopsisBean.class,reqtype);
    }
    public void getCollegeScenery(String _param, final HttpCallback callback, int reqtype) {
        super.execute(super.getHttpService().getCollegeScenery(_param), callback, CollegeSceneryBean.class, reqtype);
    }
}
