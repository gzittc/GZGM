package com.gzittc.gzgm.gm.model.db.bean.Case;

/**
 * Created by yzc on 2018-04-08.
 */

public class Judge {
    private int type;
    private int num;
    private String title;
    private String a;
    private String b;
    private String answer;
    private int part;

    public Judge(int type, int num, String title, String a, String b, String answer, int part) {
        this.type = type;
        this.num = num;
        this.title = title;
        this.a = a;
        this.b = b;
        this.answer = answer;
        this.part = part;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getA() {
        return a;
    }

    public void setA(String a) {
        this.a = a;
    }

    public String getB() {
        return b;
    }

    public void setB(String b) {
        this.b = b;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public int getPart() {
        return part;
    }

    public void setPart(int part) {
        this.part = part;
    }
}
