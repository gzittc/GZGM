package com.gzittc.gzgm.gm.model.db.bean;

import org.litepal.crud.DataSupport;

/**
 * 材料选择题
 * Created by yzc on 2017-12-16.
 */

public class SingleSubject extends DataSupport {
    private int subject_id;//题目的id
    private int subject_type;//当前题目所属材料的id
    private String main_title;//题目的内容
    private String a;//选项
    private String b;//选项
    private String c;//选项
    private String d;//选项
    private String e;//选项
    private String f;//选项
    private String answer;//题目的正确答案
    private int item_num;//当前题目有多少答案选项
    private int image_id;//题目图片，若-1则没有图片，若有则是当前id所指向的图片id
    private int explain_id;//题目对答案有没有解释，-1为没有，有则对应Explain表中的id查询

    public int getSubject_id() {
        return subject_id;
    }

    public void setSubject_id(int subject_id) {
        this.subject_id = subject_id;
    }

    public int getSubject_type() {
        return subject_type;
    }

    public void setSubject_type(int subject_type) {
        this.subject_type = subject_type;
    }

    public String getMain_title() {
        return main_title;
    }

    public void setMain_title(String main_title) {
        this.main_title = main_title;
    }

    public String getA() {
        return a;
    }

    public void setA(String a) {
        this.a = a;
    }

    public String getB() {
        return b;
    }

    public void setB(String b) {
        this.b = b;
    }

    public String getC() {
        return c;
    }

    public void setC(String c) {
        this.c = c;
    }

    public String getD() {
        return d;
    }

    public void setD(String d) {
        this.d = d;
    }

    public String getE() {
        return e;
    }

    public void setE(String e) {
        this.e = e;
    }

    public String getF() {
        return f;
    }

    public void setF(String f) {
        this.f = f;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public int getItem_num() {
        return item_num;
    }

    public void setItem_num(int item_num) {
        this.item_num = item_num;
    }

    public int getImage_id() {
        return image_id;
    }

    public void setImage_id(int image_id) {
        this.image_id = image_id;
    }

    public int getExplain_id() {
        return explain_id;
    }

    public void setExplain_id(int explain_id) {
        this.explain_id = explain_id;
    }
}
