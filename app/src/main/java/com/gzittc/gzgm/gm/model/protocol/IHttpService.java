package com.gzittc.gzgm.gm.model.protocol;

import com.google.gson.JsonObject;
import com.gzittc.gzgm.common.base.Const;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * 请求接口
 * @author YZC
 * created at 2017-12-09
 */
public interface IHttpService {
    String HOST = Const.HOST_IP;
    String HOST_URL = "http://gm.cchtw.com";
    int GET_HOME_BANNER = 1;
    int GET_HOME_QUESTION = 2;
    int GET_MAJOR_LIST=3;
    int GET_MAJOR_MESSAGE=5;
    int GET_COllEGE_SYNOPSIS = 6;


    //首页banner  reqtype=1
    @FormUrlEncoded
    @POST("?")
    Call<JsonObject> getHomeBanner(@Field("_param") String param);

    //首页问答 reqtype=2
    @FormUrlEncoded
    @POST("?")
    Call<JsonObject> getHomeQuestion(@Field("_param") String param);

    //首页考试图片  Examination reqtype=3
    @FormUrlEncoded
    @POST("?")
    Call<JsonObject> getHomeExamination(@Field("_param") String param);

    //学校专业列表 reqtype=4
    @FormUrlEncoded
    @POST("?")
    Call<JsonObject> getMajorList(@Field("_param") String param);

    //学校专业详细信息 reqtype=5
    @FormUrlEncoded
    @POST("?")
    Call<JsonObject> getMajorMessage(@Field("_param") String param);

    //学校简介信息 reqtype=6
    @FormUrlEncoded
    @POST("?")
    Call<JsonObject> getCollegeSynopsis(@Field("_param") String param);

    //学院风景 reqtype=7
    @FormUrlEncoded
    @POST("?")
    Call<JsonObject> getCollegeScenery(@Field("_param") String param);

}
