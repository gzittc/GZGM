package com.gzittc.gzgm.gm.ui.fragment;

import android.os.Message;
import android.view.View;
import android.widget.LinearLayout;

import com.gzittc.gzgm.R;
import com.gzittc.gzgm.common.base.BaseFragment;
import com.gzittc.gzgm.common.base.Global;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by Administrator on 2017/12/12.
 */

public class CommunityFragment extends BaseFragment {
    @BindView(R.id.parent_ll)
    LinearLayout parentLl;
    Unbinder unbinder;

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_community;
    }

    @Override
    public void initView() {
        ButterKnife.bind(this,mRoot);
        Global.setStatusBarPadding(parentLl);
    }

    @Override
    public void initListener() {

    }

    @Override
    public void initData() {

    }

    @Override
    public void onClick(View v, int id) {

    }

    @Override
    public void onHttpSuccess(int reqtype, Message msg) {

    }

    @Override
    public void onHttpError(String error) {

    }


}
