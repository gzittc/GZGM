package com.gzittc.gzgm.gm.presenter;

import android.content.Context;

import com.gzittc.gzgm.gm.model.db.DBHelper.DBHelper;
import com.gzittc.gzgm.gm.model.db.DBHelper.PSexamHelper;

import org.litepal.crud.DataSupport;

/**
 * Created by Administrator on 2018/1/4.
 */

public class MainActivityPresenter {

    public void processingPDTestDatabase(Context context){
        DBHelper dbHelper = new DBHelper(context);
        DataSupport.saveAll(dbHelper.getPDQuestion());
        new Thread(new Runnable() {
            @Override
            public void run() {
                new PSexamHelper();
            }
        }).start();
    }
}
