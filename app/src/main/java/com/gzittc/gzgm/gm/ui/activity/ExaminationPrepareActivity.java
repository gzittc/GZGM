package com.gzittc.gzgm.gm.ui.activity;

import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.gzittc.gzgm.R;
import com.gzittc.gzgm.common.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2017/12/22.
 */

public class ExaminationPrepareActivity extends BaseActivity {
    @BindView(R.id.prepare_examination)
    Button prepare;
    @BindView(R.id.tv_major)
    TextView tvMajor;
    @BindView(R.id.tv_content)
    TextView tvContent;
    @BindView(R.id.tv_time)
    TextView tvTime;
    @BindView(R.id.tv_many)
    TextView tvMany;
    @BindView(R.id.editText)
    EditText number;

    private String sql = "";
    private int time;

    @Override
    public int getLayoutRes() {
        return R.layout.activity_examination_prepare;
    }

    @Override
    public void initView() {
        ButterKnife.bind(this);
        Intent intent = getIntent();
        sql = intent.getStringExtra("sql");
        tvMajor.setText(intent.getStringExtra("subject"));
        tvContent.setText(intent.getStringExtra("content"));
        time = Integer.parseInt(intent.getStringExtra("time"));
        tvTime.setText(time+"分钟");
        tvMany.setText(intent.getStringExtra("many"));
    }

    @Override
    public void initData() {

    }

    @Override
    public void onClick(View v, int id) {
        switch (id) {
            case R.id.prepare_examination:
//                showDialog();
                if (number.getText().toString().length()!= 2 || TextUtils.isEmpty(number.getText())){
                    Toast.makeText(this, "学号不正确", Toast.LENGTH_SHORT).show();
                    break;
                }else {
                    Intent intent = new Intent(this, ExamStartActivity.class);
                    intent.putExtra("sql",sql);
                    intent.putExtra("time",time);
                    intent.putExtra("number", number.getText().toString());
                    startActivity(intent);
                    finish();
                    break;
                }
        }
    }

    @Override
    public void initListener() {
        super.initListener();
        prepare.setOnClickListener(this);
    }

}
