package com.gzittc.gzgm.gm.model.db.bean;

import org.litepal.crud.DataSupport;

/**
 * 多选题
 * Created by yzc on 2017-12-16.
 */

public class MoreSubject extends DataSupport {
    private int subject_id;//题目的id
    private String main_title;//题目
    private String a;//A答案
    private String b;//A答案
    private String c;//A答案
    private String d;//A答案
    private String e;//A答案
    private String f;//A答案
    private String g;//A答案
    private String answer;//答案
    private int item_num;//当前题目有多少答案选项
    private int main_title_len;//题目长度

    public int getSubject_id() {
        return subject_id;
    }

    public void setSubject_id(int subject_id) {
        this.subject_id = subject_id;
    }

    public String getMain_title() {
        return main_title;
    }

    public void setMain_title(String main_title) {
        this.main_title = main_title;
    }

    public String getA() {
        return a;
    }

    public void setA(String a) {
        this.a = a;
    }

    public String getB() {
        return b;
    }

    public void setB(String b) {
        this.b = b;
    }

    public String getC() {
        return c;
    }

    public void setC(String c) {
        this.c = c;
    }

    public String getD() {
        return d;
    }

    public void setD(String d) {
        this.d = d;
    }

    public String getE() {
        return e;
    }

    public void setE(String e) {
        this.e = e;
    }

    public String getF() {
        return f;
    }

    public void setF(String f) {
        this.f = f;
    }

    public String getG() {
        return g;
    }

    public void setG(String g) {
        this.g = g;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public int getItem_num() {
        return item_num;
    }

    public void setItem_num(int item_num) {
        this.item_num = item_num;
    }

    public int getMain_title_len() {
        return main_title_len;
    }

    public void setMain_title_len(int main_title_len) {
        this.main_title_len = main_title_len;
    }
}
