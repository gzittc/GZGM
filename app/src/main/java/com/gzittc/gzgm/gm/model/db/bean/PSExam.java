package com.gzittc.gzgm.gm.model.db.bean;

import org.litepal.crud.DataSupport;

/**
 * Created by yzc on 2018-01-07.
 */

public class PSExam extends DataSupport {
    private int id;//题号
    private String type;//题目类型 单选
    private String title;//题目
    private String A;//A答案
    private String B;//
    private String C;//
    private String D;//
    private String answer;//本体答案

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getA() {
        return A;
    }

    public void setA(String a) {
        A = a;
    }

    public String getB() {
        return B;
    }

    public void setB(String b) {
        B = b;
    }

    public String getC() {
        return C;
    }

    public void setC(String c) {
        C = c;
    }

    public String getD() {
        return D;
    }

    public void setD(String d) {
        D = d;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

}
